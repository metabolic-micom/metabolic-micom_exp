#######################
#######################
# May 27, 2021
# Coculture simulations
# List of selected species was manually created
# Species were selected & classified based on "ecological property" of how much the effect of plasticity is in different mono-culture conditions
# batch_simulationTime = 12.0
# continuous_simulationTime = 96.0
# program version: v2.10.0
#######################
# perturbed from 96.0 to 97.0h
# perturbation = lysing of 50% population of 1 species
#######################
# species: with ecotypes I, II, and V
# media: EU_avg & high_fat
#######################
####################### 

# Arrays of selected species
species_list="./selected_species_setA.tsv"

mapfile -t ecotype_arr < <(tail -n +2 $species_list | cut -f1)
mapfile -t ecotypeID_arr < <(tail -n +2 $species_list | cut -f2)
mapfile -t speciesID_arr < <(tail -n +2 $species_list | cut -f3)
mapfile -t species_arr < <(tail -n +2 $species_list | cut -f4)

#######################
# Prepare calibration files
# Copy from `experiments/calib_setA`. Use calibration in EU_avg
# Change FLX and YLD of all species to the same desired values
mkdir v2.10.0_growth_parameters_EU_avg/

# Check desired FLX and YLD values, using R script
#conda activate micom # Activate environment with R=3.5.1
../calib_setA/find_FLX_YLD.R

# Change FLX to chosen `FLX` = maximum `FLX` calibrated among all diets and all species = 2246.13
# and Change YLD to chosen `YLD` = value corresponding to the chosen `FLX` = 0.0837152. 
# <=> `FLX` and `YLD` of the most struggling species in its most difficult environment (difficulty = how high biomass production rate can be).

chosen_flx=2246.13;
chosen_yld=0.0837152;

for species in ${species_arr[@]}; do
        
    echo "Preparing calibration directory for species: "$species" ...";
    
    calib_dir="v2.10.0_growth_parameters_EU_avg/"$species;
    cp -r "../calib_setA/"$calib_dir "./"$calib_dir;
    
    echo "Changing FLX and YLD";
    
    calib_file="./"$calib_dir"/growth_curve_parameters.tsv";
        
    awk -v flx=$chosen_flx -v yld=$chosen_yld '{IFS="\t"; OFS="\t"} {if ($1=="FLX") print $1,flx; else if ($1=="YLD") print $1,yld; else print $1,$2}' $calib_file > tmp.$species.tsv;
    
    mv tmp.$species.tsv $calib_file;
        
done

# Checking
head -n5 "./v2.10.0_growth_parameters_EU_avg/"*"/growth_curve_parameters.tsv"

#######################
# Prepare parameter files
param_dir=$PWD"/parameters"

species_list=""
ecotypeID_list=""
speciesID_list=""
for i in ${!species_arr[@]}; do
    
    ecotype=${ecotype_arr[$i]};
    if [ $ecotype == "I" ] || [ $ecotype == "II" ] || [ $ecotype == "V" ]; then
        species_list=$species_list" "${species_arr[$i]};
        ecotypeID_list=$ecotypeID_list" "${ecotypeID_arr[$i]};
        speciesID_list=$speciesID_list" "${speciesID_arr[$i]};
    fi;

done

# EU_avg should always be the first media, because we're using calibration files in EU_avg
media_list="EU_avg high_fat"

mode_list="plastic sfm"  

./make_parameters "$species_list" "$ecotypeID_list" "$speciesID_list" "$media_list" "$mode_list" $param_dir

####################### 
# Running & Replicating (on Peregrine cluster)
# 1 job for 1 parameter file - coculture of 1 pair in all media - in either batch or continuous culture
# regular partition, 1 node, 1 task, 1 CPU/task, total mem = 4GB
# Herein, $COMMUNISM_JOB is an environment variable storing full path to the jobscript `communism_job.sh`

param_dir=$PWD"/parameters"
calib_dir=$PWD"/v2.10.0_growth_parameters_EU_avg"
control_dir=$MICOM_EXP"setA_coculture_b12_c96"

# Testing
# ./coculture $param_dir"/param_I_b_II_a_sfm.txt" $calib_dir sfm rep1 "coculture_continuous"

run_moment=$(date '+%d%m%Y_%H:%M:%S') # 30062021_14:53:07

p="0" # Perturb 1st species in parameter files
for i in {1..10}; do
    rep_name="rep"$i;
    
    if [ $i -lt 4 ]; then
        tail=".txt"; # run in both modes plastic & sfm
    else
        tail="plastic.txt"; # run in plastic mode only
    fi;

    for param_file in $param_dir"/param_"*$tail; do # run for all commcommunities found in the parameters directory
        param_filename=${param_file%.*};
        mode=${param_filename##*_};
        
        ./coculture_perturbed $param_file $calib_dir $mode $rep_name $control_dir $p;
        echo -e "\n============================================\n";
    done;
    
done > run.$run_moment.log 2>&1

# Wait until the above experiments have been running for ~5-10min
# then submit jobs for perturbing 2nd species in parameter files
# DO NOT repeat copying random seeds from mono-culture exp

p="1"
for i in {1..10}; do
    rep_name="rep"$i;
    
    if [ $i -lt 4 ]; then
        tail=".txt"; # run in both modes plastic & sfm
    else
        tail="plastic.txt"; # run in plastic mode only
    fi;

    for param_file in $param_dir"/param_"*$tail; do # run for all commcommunities found in the parameters directory
        param_filename=${param_file%.*};
        mode=${param_filename##*_};
        
        ./coculture_perturbed $param_file $calib_dir $mode $rep_name "" $p; 
        echo -e "\n============================================\n";
    done;
    
done >> run.$run_moment.log 2>&1

#######################
# Check hardcoded_parameters
rep_name_list=""
for i in {1..10}; do
    rep_name_list=$rep_name_list" rep"$i;
done

source ./checking_functions.sh
hardcoded_parameters "$rep_name_list"

#Check
grep 'plastic' hardcoded_parameters.tsv | cut -f4 | sort | uniq
grep 'sfm' hardcoded_parameters.tsv | cut -f4 | sort | uniq
cut -f9,10 hardcoded_parameters.tsv | sort | uniq

#######################
# Check running status
rep_name_list=""
for i in {1..10}; do
    rep_name_list=$rep_name_list" rep"$i;
done

source ./checking_functions.sh
run_status "$rep_name_list"

#Check
tail -n +2 run_status.tsv | cut -f1,6 | sort | uniq
