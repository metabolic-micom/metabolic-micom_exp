#######################
#######################
# Apr 29, 2021
# This script stores functions for post-simulations checking. They produce summary files of running parameters.
#######################
#######################

#######################
# Produce hardcoded_parameters.tsv that contains all hardcoded parameters of all simulations of all the replicates.
# Take input as a "list" of names of replicates.
# Example: ```list="rep1 rep2 rep3"; hardcoded_parameters "$list";```

function hardcoded_parameters(){
    local rep_name_arr=($1);
    
    echo -e rep_name"\t"simID"\t"flowRate"\t"NFluxModes"\t"fluxNGAM"\t"SwitchRate"\t"SwitchSensitivity"\t"numReps"\t"batch_simTime"(h)\t"continuous_simTime"(h)\t""initDens(a.u.)" > hardcoded_parameters.tsv;

    for rep_name in ${rep_name_arr[@]}; do
        
        local log_dir="./"$rep_name"/log";
        
        for log_file in $log_dir"/"*".log"; do
            local log_filename=${log_file##*/};
            local simID=${log_filename%%.*};

            local sim_dir="./"$rep_name"/v2.10.0_sim_"$simID;
            local data=$(tail -n +2 $sim_dir"/hardcoded_parameters.tsv" | sed -e 's/\t/\\t/g');
            
            echo -e $rep_name"\t"$simID"\t"$data >> hardcoded_parameters.tsv;
        done; 
    done;
}

#######################
# Produce run_status.tsv that contains all running status parameters of all simulations of all the replicates.
# Take input as a "list" of names of replicates.
# Example: ```list="rep1 rep2 rep3"; run_status "$list";```

function run_status(){
    local rep_name_arr=($1);
    
    echo -e rep_name"\t"simID"\t"perturbed_sp"\t"program_log_doneSim"\t"program_log_doneR"\t"slurm_log"\t"run_time"\t"max_mem_used > run_status.tsv;

    for rep_name in ${rep_name_arr[@]}; do
        
        local log_dir="./"$rep_name"/log";
        local slurm_dir="./"$rep_name"/slurm_log";
        local job_file="./"$rep_name"/jobs.tsv";
        
        for log_file in $log_dir"/"*".log"; do
        
            local log_filename=${log_file##*/};
            # separate elements of `log_filename` into an array, elements separated by "."
            IFS="." read -r -a log_filename_arr <<< "$log_filename"
            local simID=${log_filename_arr[0]};
            local perturbed_sp=${log_filename_arr[2]};
            
            local program_log_doneSim=$(grep -c "*** All simulations finished ***" $log_file)
            
            local check=$(tail -n 4 $log_file | head -n 1);
            if [[ $check == "*** Finished calling R scripts ***" ]]; then
                local program_log_doneR=1;
            else
                local program_log_doneR=0;
            fi;
            
            local slurm_id=$(grep "log/${log_filename}" $job_file | cut -f1);
            local slurm_log_file=$slurm_dir"/"$slurm_id".log";
            local slurm_log=$(grep "State" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
            
            local run_time=$(grep "Used walltime" $slurm_log_file | cut -d ":" --output-delimiter ":" -f2,3,4 | tr -d " ");
            
            local max_mem_used=$(grep "Max Mem used" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
            
            echo -e $rep_name"\t"$simID"\t"$perturbed_sp"\t"$program_log_doneSim"\t"$program_log_doneR"\t"$slurm_log"\t"$run_time"\t"$max_mem_used >> run_status.tsv;
        
        done;
    done;
}

