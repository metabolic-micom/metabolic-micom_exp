# On my local machine
# Copied directories from `setA_coculture_b12_c96`: `v2.10.0_growth_parameters_EU_avg`

# Arrays of selected species
species_list="./selected_species_setA.tsv"

mapfile -t ecotype_arr < <(tail -n +2 $species_list | cut -f1)
mapfile -t ecotypeID_arr < <(tail -n +2 $species_list | cut -f2)
mapfile -t speciesID_arr < <(tail -n +2 $species_list | cut -f3)
mapfile -t species_arr < <(tail -n +2 $species_list | cut -f4)

#######################
# Prepare parameter files
param_dir=$PWD"/parameters"

species_list=""
ecotypeID_list=""
speciesID_list=""
for i in ${!species_arr[@]}; do
    
    ecotype=${ecotype_arr[$i]};
    if [ $ecotype == "I" ] || [ $ecotype == "II" ] || [ $ecotype == "V" ]; then
        species_list=$species_list" "${species_arr[$i]};
        ecotypeID_list=$ecotypeID_list" "${ecotypeID_arr[$i]};
        speciesID_list=$speciesID_list" "${speciesID_arr[$i]};
    fi;

done

# EU_avg should always be the first media, because we're using calibration files in EU_avg
media_list="EU_avg high_fat"

mode_list="plastic sfm"  

./make_parameters.local "$species_list" "$ecotypeID_list" "$speciesID_list" "$media_list" "$mode_list" $param_dir

#######################
# Running on Continuous culture
coculture_type="coculture_continuous"

param_dir=$PWD"/parameters"
log_dir=$PWD"/log"

program=$MICOM"/program/communism"

perturbed_list="A B"

# Program is on branch "v2.10.0_sfm.perturbed"
# RKTolerance = 1.0e-5
mode="sfm"

community="I_a_II_b"
for p in $perturbed_list; do
    simID=$community"_"$mode"_"$p;
    param_file=$param_dir"/param_"$simID".txt";
    log_file=$log_dir"/"$simID".log";
    $program $param_file $coculture_type > $log_file 2>&1; 
done

community="I_a_I_b"
for p in $perturbed_list; do
    simID=$community"_"$mode"_"$p;
    param_file=$param_dir"/param_"$simID".txt";
    log_file=$log_dir"/"$simID".log";
    $program $param_file $coculture_type > $log_file 2>&1; 
done

# Program is on branch "v2.10.0_plastic.perturbed"
# RKTolerance = 1.0e-5
mode="plastic"

community="I_a_II_b"
for p in $perturbed_list; do
    simID=$community"_"$mode"_"$p;
    param_file=$param_dir"/param_"$simID".txt";
    log_file=$log_dir"/"$simID".log";
    $program $param_file $coculture_type > $log_file 2>&1; 
done

community="I_a_I_b"
for p in $perturbed_list; do
    simID=$community"_"$mode"_"$p;
    param_file=$param_dir"/param_"$simID".txt";
    log_file=$log_dir"/"$simID".log";
    $program $param_file $coculture_type > $log_file 2>&1; 
done


