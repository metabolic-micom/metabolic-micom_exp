import pandas as pd
import numpy as np

import gc # Garbage Collector for releasing unreferenced memory

# from my_modules import compute

from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.collections
import matplotlib.patches as patches
import seaborn as sns

sns.set_style("whitegrid")

font = {'family': 'sans-serif', 'serif' : 'Helvetica',
        'size'   : 25}
plt.rc('font', **font)

my_quali_colormaps = ['colorblind', 'Dark2']
final_OD_palette = {'ON': '#ffb219', 'OFF': '#a1a9c5'}

#################################################
# Plotting bacteria density (OD) over time

def plot_OD_over_time(storage, community_mode, replicates, media, save_fig=True, fig_dir="./", fig_name=None):

    # Monitor running process & validate arguments
    status = 0
    
    check_culture_type = set(r.rsplit('_')[1] for r in replicates)

    if len(check_culture_type) > 1:
        raise ValueError('Function only works for one type of culture, either batch or continuous')

    # Metadata of species names & ecotypes
    meta_file = storage + 'selected_species_setA.tsv'
    meta = pd.read_csv(meta_file, sep="\t")
    
    perturbed_order = ['A', 'B']

    # Plotting
    fig = plt.figure(figsize=(14*2, 15*len(media)))

    outer = gridspec.GridSpec(nrows=len(media), ncols=2, wspace=0.2, hspace=0.2)

    for i in range(len(media)):
        for p in range(len(perturbed_order)):
            
            merged_data = pd.DataFrame()
            for r in replicates:
                rep = r.rsplit('_')[0]

#                 data_dir = storage + rep + '/v2.10.0_sim_' + community_mode + '_' + perturbed_order[p] + '/experiments/' + list(check_culture_type)[0] + '_coculture/'  + media[i] + '/'
                data_dir = storage + 'v2.10.0_sim_' + community_mode + '_' + perturbed_order[p] + '/experiments/' + list(check_culture_type)[0] + '_coculture/'  + media[i] + '/'
                data_file = data_dir + 'species_trajectory.tsv'

                df = pd.read_csv(data_file, sep="\t", usecols=['species_name', 'time', 'od'])
                df['replicate'] = rep
                
                merged_data = pd.concat([merged_data, df])
            
            if p == 0:
                members = meta.loc[meta['Species_name'].isin(df['species_name'].unique()), :]
                
            merged_data.reset_index(drop=True)
            
            inner = gridspec.GridSpecFromSubplotSpec(nrows=members.shape[0], ncols=1,
                                                     subplot_spec=outer[i, p], wspace=0.15, hspace=0.15)

            for j in range(members.shape[0]):

                plot_data = merged_data.loc[merged_data['species_name'] == members.iloc[j, 3], ['time', 'od', 'replicate']]

                plot_ax = plt.Subplot(fig, inner[j])

                kwargs={'linewidth':3}
                g = sns.lineplot(data = plot_data, x='time', y='od', hue='replicate', ax=plot_ax, 
                                 palette = sns.color_palette(my_quali_colormaps[1], len(replicates)), 
                                 **kwargs)
                title = members.iloc[j, 1] + " (" + members.iloc[j, 2] + ")" 
                plot_ax.set(title=title, ylabel='OD')
                
                h = plot_ax.get_ylim()[1]
                perturb_area = patches.Rectangle(xy=(96.0, 0.0), height=h, width=1.0, color='gray', alpha=0.3, fill=True)
                plot_ax.add_patch(perturb_area)

                if j == members.shape[0]-1:
                    plot_ax.set(xlabel='Time (h)')
                else:
                    plot_ax.set(xlabel='', xticklabels=[])

                if j > 0:
                    plot_ax.get_legend().remove()
                elif p == 0:
                    plot_ax.get_legend().remove()
                    plot_ax.text(0.0, 1.1, s=media[i], ha='center', va='center', transform=plot_ax.transAxes, 
                                 fontweight='bold')
                else:
                    lgnd = plot_ax.legend(loc='upper left', bbox_to_anchor=(1.0, 1.0), title='Replicate')
                    # Change width of lines in legend
                    for line in lgnd.get_lines():
                        line.set_linewidth(5)

                fig.add_subplot(plot_ax)
    
    # Save figure
    if save_fig:
        fig_name = fig_dir + community_mode + "." + list(check_culture_type)[0] + ".OD_over_time" + ".svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close("all")
        plt.clf()

        status = fig_name + ": Plotted & Saved."
    else:
        status = 1
        
    # Freeing memory usage
    del df, merged_data, plot_data
    del fig
    gc.collect()
    
    return status