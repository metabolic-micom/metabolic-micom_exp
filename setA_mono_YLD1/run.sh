#######################
#######################
# Feb 23, 2021
# Run plastic and non-plastic monoculture experiments on set A, tested in all diets
# Use program version 2.10.0, on branch "v2.10.0_plastic" and branch "v2.10.0_sfm"
#######################
#######################

#######################
# Get list of species
# Select only species that can be calibrated
# As has been observed in `calib_setA`, once a species cannot be calibrated in one environment, it also cannot be calibrated in all other environments
# Pick calib folder in EU_avg as the representative `calib_dir`
large_species_list="../selected_species_sets.tsv"
species_list="./selected_species_setA.tsv"

echo -e Species_ID"\t"Species_name > $species_list

calib_dir="../calib_setA/v2.10.0_growth_parameters_EU_avg"

while IFS=$'\t' read -r set species_numb species; do #run through every species
    if [[ $set == "A" ]]; then
        if [ -d $calib_dir"/"$species ]; then #check if directory for calibration parameters exists
            echo -e "setA_"$species_numb"\t"$species >> $species_list;
        fi;
    fi;
done < <(tail -n +2 $large_species_list | cut -f1,2,3)

#######################
# Prepare parameter files
mkdir parameters/

species_list="./selected_species_setA.tsv"
param_dir="./parameters/"
media_dir="../materials/media"
sbml_dir="../materials/sbml_files/agora_1.03"

# EU_avg should always be the first media, because we're using calibration files in EU_avg
media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

modes="plastic sfm"        

while IFS=$'\t' read -r speciesID species; do
        
        echo "Making parameter files for species: "$speciesID" in all media...";
        
        for mode in $modes; do
            simID=$speciesID"_"$mode;
            param_file=$param_dir"/param_"$simID".txt";
        
            echo $simID > $param_file;
        
            for m in $media; do
    
                echo $media_dir"/"$m".csv" >> $param_file;
            
            done;
        
            sbml=$sbml_dir"/"$species".xml";
            echo $sbml >> $param_file;
        done;
        
done < <(tail -n +2 $species_list | cut -f1,2,3)

#######################
# Prepare calibration files
# Copy from `experiments/calib_setA`. Use calibration in EU_avg
# Change FLX and YLD of all species to the same desired values
cp -r ../calib_setA/v2.10.0_growth_parameters_EU_avg/ .

# Check desired FLX and YLD values, using R script

conda activate micom # Activate environment with R=3.5.1
../calib_setA/find_FLX_YLD.R

# Change FLX to chosen `FLX` = maximum `FLX` calibrated among all diets and all species = 2246.13
# and Change YLD to 1. chosen `YLD` = value corresponding to the chosen `FLX` = 0.0837152. 
# <=> `FLX` and `YLD` of the most struggling species in its most difficult environment (difficulty = how high biomass production rate can be).
species_list="./selected_species_setA.tsv"

while IFS=$'\t' read -r speciesID species; do
        
        echo "Changing FLX and YLD for species: "$speciesID" ...";
        
        calib_dir="./v2.10.0_growth_parameters_EU_avg/"$species;
        calib_file=$calib_dir"/growth_curve_parameters.tsv";
            
        awk '{IFS="\t"; OFS="\t"} {if ($1=="FLX") print $1,2246.13; else if ($1=="YLD") print $1,0.0837152; else print $1,$2}' $calib_file > tmp.$speciesID.tsv;
        
        mv tmp.$speciesID.tsv $calib_file;
        
done < <(tail -n +2 $species_list | cut -f1,2,3)

####################### 
# Run on Peregrine cluster
# 1 job for 1 species in 1 environment
# regular partition, 1 node, 1 task, 1 CPU/task
# Herein, $COMMUNISM is an environment variable storing the full path of the program; $COMMUNISM_JOB_4CPUS is an environment variable storing full path to the jobscript `communism_job.4cpus.sh`
cd $MICOM_EXP"setA_mono_YLD1/"
mkdir ./slurm_log/
mkdir ./log/

param_dir="./parameters"
log_dir="./log"

#####
# Plastic mode
# Use program stored in "v2.10.0_plastic"
mode="plastic"
program="COMMUNISM_2100_"${mode^^} #COMMUNISM_2100_PLASTIC

now=$(date '+%d%m%Y_%H:%M:%S') # 24022021_10:17:11
for param_file in $param_dir"/param_setA_"*$mode".txt"; do
        
    echo $param_file;
    simIDtxt=${param_file#*_};
    simID=${simIDtxt%.*};
                
    log_file=$log_dir"/"$simID"."$now".log";
        
    sbatch $COMMUNISM_JOB ${!program} $param_file $log_file; # run with 4 CPUs/task - reported as not efficient, because the program is not run multi-core
done

#####
# Non-plastic mode
# Use program stored in "v2.10.0_sfm"
mode="sfm"
program="COMMUNISM_2100_"${mode^^} #COMMUNISM_2100_SFM

now=$(date '+%d%m%Y_%H:%M:%S'); # 24022021_22:48:42
for param_file in $param_dir"/param_setA_"*$mode".txt"; do
        
    echo $param_file;
    simIDtxt=${param_file#*_};
    simID=${simIDtxt%.*};
                
    log_file=$log_dir"/"$simID"."$now".log";
        
    sbatch $COMMUNISM_JOB ${!program} $param_file $log_file; #run with 2 CPUs/task - reported as not efficient, because the program is not run multi-core

done

#####
# Check if all have run completely
slurm_dir="./slurm_log"
param_dir="./parameters"
log_dir="./log"

mode="plastic"
now="24022021_10:17:11"

mode="sfm"
now="24022021_22:48:42"

echo -e ID"\t"program_log_doneSim"\t"program_log_doneR"\t"slurm_log"\t"run_time"\t"max_mem_used > $now.status

for log_file in $log_dir"/setA_"*$mode"."$now".log"; do
    
    log=${log_file##*/};
    id=${log%%.*};
    
    program_log_doneSim=$(grep -c "*** All simulations finished ***" $log_file)
    
    check=$(tail -n 4 $log_file | head -n 1);
    if [[ $check == "*** Finished calling R scripts ***" ]]; then
        program_log_doneR=1;
    else
        program_log_doneR=0;
    fi;
    
    slurm_id=$(grep "$log_file" jobs.tsv | cut -f1);
    slurm_log_file=$slurm_dir"/"$slurm_id".log";
    slurm_log=$(grep "State" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
    
    run_time=$(grep "Used walltime" $slurm_log_file | cut -d ":" --output-delimiter ":" -f2,3,4 | tr -d " ");
    
    max_mem_used=$(grep "Max Mem used" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
    
    echo -e $id"\t"$program_log_doneSim"\t"$program_log_doneR"\t"$slurm_log"\t"$run_time"\t"$max_mem_used >> $now.status;
                       
done

####################### 
# Preliminary analysis

#####
# Get species that simulations have been done
log_dir="./log"

mode="plastic"
now="24022021_10:17:11"

mode="sfm"
now="24022021_22:48:42"

done_list=$mode.$now.done
awk '{IFS="\t"} {if ($2==1) print $1}' $now.status > $done_list

#####
# Extract final point data
media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

time_file="./"$mode"."$now".batch_mono-culture.final_point.time.tsv"
od_file="./"$mode"."$now".batch_mono-culture.final_point.od.tsv"
header="simID"
for m in $media; do
    header=$header"\t"$m;
done
echo -e $header > $time_file
echo -e $header > $od_file

while read -r id; do
    
    log_file=$log_dir"/"$id"."$now".log";
    
    time=$id;
    od=$id;
    
    for m in $media; do
        pattern="batch_mono-culture with "$m" diet";
        
        add_time=$(grep -A4 "$pattern" $log_file | awk '{FS=" = "} NR==3 {gsub("h","",$2); print $2}');
        time=$time"\t"$add_time;
        
        add_od=$(grep -A4 "$pattern" $log_file | awk '{FS=";"} NR==4 {print $1}' | awk '{FS="="} {print $3}');
        od=$od"\t"$add_od;
    done;
    
    echo -e $time >> $time_file;
    echo -e $od >> $od_file;
    
done < $done_list

# Cases that reached 12h
grep -w "12" $time_file
# setA_16_plastic   2.17101 12  2.31166 12  12  10.6319 10.6614 10.6433 12  10.6616 12
# and
# setA_16_sfm     1.97299 12      1.91316 12      12      10.6337 10.6598 10.6455 12      10.6644 12
# Inspecting growth curve graphs manually, found that these cases likely have reached stationary phase

