#######################
#######################
# Mar 09, 2021
# Run plastic (~~and non-plastic~~) monoculture experiments on set A, tested in all diets
# Use program version 2.10.0, on branch "v2.10.0_plastic" and branch "v2.10.0_sfm"
# To save less data points, in Environment::simulateTrajectory(...), always output=false
# Simulation time extended to 42h for species to reach stationary phase
# `dtMax = delta * 24.0` => Obtain at least 1000 data points per 24h
#######################
#######################

#######################
# Get list of species
# Select only species that can be calibrated
# As has been observed in `calib_setA`, once a species cannot be calibrated in one environment, it also cannot be calibrated in all other environments
# Pick calib folder in EU_avg as the representative `calib_dir`
large_species_list="../selected_species_sets.tsv"
species_list="./selected_species_setA.tsv"

echo -e Species_ID"\t"Species_name > $species_list

calib_dir="../calib_setA/v2.10.0_growth_parameters_EU_avg"

while IFS=$'\t' read -r set species_numb species; do #run through every species
    if [[ $set == "A" ]]; then
        if [ -d $calib_dir"/"$species ]; then #check if directory for calibration parameters exists
            echo -e "setA_"$species_numb"\t"$species >> $species_list;
        fi;
    fi;
done < <(tail -n +2 $large_species_list | cut -f1,2,3)

#######################
# Prepare parameter files
mkdir parameters/

species_list="./selected_species_setA.tsv"
param_dir="./parameters/"
media_dir="../materials/media"
sbml_dir="../materials/sbml_files/agora_1.03"

# EU_avg should always be the first media, because we're using calibration files in EU_avg
media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

modes="plastic sfm"        

while IFS=$'\t' read -r speciesID species; do
        
        echo "Making parameter files for species: "$speciesID" in all media...";
        
        for mode in $modes; do
            simID=$speciesID"_"$mode;
            param_file=$param_dir"/param_"$simID".txt";
        
            echo $simID > $param_file;
        
            for m in $media; do
    
                echo $media_dir"/"$m".csv" >> $param_file;
            
            done;
        
            sbml=$sbml_dir"/"$species".xml";
            echo $sbml >> $param_file;
        done;
        
done < <(tail -n +2 $species_list | cut -f1,2,3)

#######################
# Prepare calibration files
# Copy from `experiments/calib_setA`. Use calibration in EU_avg
# Change FLX and YLD of all species to the same desired values
cp -r ../calib_setA/v2.10.0_growth_parameters_EU_avg/ .

# Check desired FLX and YLD values, using R script

#conda activate micom # Activate environment with R=3.5.1
../calib_setA/find_FLX_YLD.R

# Change FLX to chosen `FLX` = maximum `FLX` calibrated among all diets and all species = 2246.13
# and Change YLD to chosen `YLD` = value corresponding to the chosen `FLX` = 0.0837152. 
# <=> `FLX` and `YLD` of the most struggling species in its most difficult environment (difficulty = how high biomass production rate can be).
species_list="./selected_species_setA.tsv"

while IFS=$'\t' read -r speciesID species; do
        
        echo "Changing FLX and YLD for species: "$speciesID" ...";
        
        calib_dir="./v2.10.0_growth_parameters_EU_avg/"$species;
        calib_file=$calib_dir"/growth_curve_parameters.tsv";
            
        awk '{IFS="\t"; OFS="\t"} {if ($1=="FLX") print $1,2246.13; else if ($1=="YLD") print $1,0.0837152; else print $1,$2}' $calib_file > tmp.$speciesID.tsv;
        
        mv tmp.$speciesID.tsv $calib_file;
        
done < <(tail -n +2 $species_list | cut -f1,2,3)

####################### 
# Running (on Peregrine cluster)
# 1 job for 1 species in 1 environment
# regular partition, 1 node, 1 task, 1 CPU/task
# Herein, $COMMUNISM_2100_PLASTIC and $COMMUNISM_2100_SFM are environment variables storing the full path of the program; $COMMUNISM_JOB is an environment variable storing full path to the jobscript `communism_job.sh`

mkdir ./slurm_log/
mkdir ./log/

param_dir="./parameters"
log_dir="./log"

#####
# Plastic mode
# Use program stored in "v2.10.0_plastic"
mode="plastic"
program="COMMUNISM_2100_"${mode^^} #COMMUNISM_2100_PLASTIC

now="rep1"
for param_file in $param_dir"/param_setA_"*$mode".txt"; do
        
    echo $param_file;
    simIDtxt=${param_file#*_};
    simID=${simIDtxt%.*};
                
    log_file=$log_dir"/"$simID"."$now".log";
        
    sbatch $COMMUNISM_JOB ${!program} $param_file $log_file; # 1 CPU/task + total mem = 4GB. 
    
done

#####
# Non-plastic mode
# Use program stored in "v2.10.0_sfm"
# mode="sfm"
# program="COMMUNISM_2100_"${mode^^} #COMMUNISM_2100_SFM
# 
# now=$(date '+%d%m%Y_%H:%M:%S'); # 03032021_23:27:20
# for param_file in $param_dir"/param_setA_"*$mode".txt"; do
#         
#     echo $param_file;
#     simIDtxt=${param_file#*_};
#     simID=${simIDtxt%.*};
#                 
#     log_file=$log_dir"/"$simID"."$now".log";
#         
#     sbatch $COMMUNISM_JOB ${!program} $param_file $log_file; 
#     
# done

#####
# Check if all have run completely
slurm_dir="./slurm_log"
param_dir="./parameters"
log_dir="./log"

mode="plastic"
now="rep1"

# mode="sfm"
# now="03032021_23:27:20"

echo -e ID"\t"program_log_doneSim"\t"program_log_doneR"\t"slurm_log"\t"run_time"\t"max_mem_used > $now.status.tsv

for log_file in $log_dir"/setA_"*$mode"."$now".log"; do
    
    log=${log_file##*/};
    id=${log%%.*};
    
    program_log_doneSim=$(grep -c "*** All simulations finished ***" $log_file)
    
    check=$(tail -n 4 $log_file | head -n 1);
    if [[ $check == "*** Finished calling R scripts ***" ]]; then
        program_log_doneR=1;
    else
        program_log_doneR=0;
    fi;
    
    slurm_id=$(grep "$log_file" jobs.tsv | cut -f1);
    slurm_log_file=$slurm_dir"/"$slurm_id".log";
    slurm_log=$(grep "State" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
    
    run_time=$(grep "Used walltime" $slurm_log_file | cut -d ":" --output-delimiter ":" -f2,3,4 | tr -d " ");
    
    max_mem_used=$(grep "Max Mem used" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
    
    echo -e $id"\t"$program_log_doneSim"\t"$program_log_doneR"\t"$slurm_log"\t"$run_time"\t"$max_mem_used >> $now.status.tsv;
                       
done

#####
# Check hardcoded_parameters
log_dir="./log"

mode="plastic"
now="rep1"

echo -e ID"\t"flowRate"\t"NFluxModes"\t"fluxNGAM"\t"SwitchRate"\t"SwitchSensitivity"\t"numReps"\t"simTime"(h)\t""initDens(a.u.)" > $now.hardcoded_parameters.tsv

for log_file in $log_dir"/setA_"*$mode"."$now".log"; do
    log=${log_file##*/};
    id=${log%%.*}
    
    sim_dir="v2.10.0_sim_"$id;
    data=$(tail -n +2 $sim_dir"/hardcoded_parameters.tsv" | sed -e 's/\t/\\t/g');
    
    echo -e $id"\t"$data >> $now.hardcoded_parameters.tsv;
done

####################### 
# Check number of data points
log_dir="./log"

cultures="batch continuous"

media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

header="ID"
for m in $media; do
    header=$header"\t"$m;
done

for cult in $cultures; do
    output="data_points."$cult"_mono-culture.tsv";
    echo -e $header > $output;

    for log_file in $log_dir/*.log; do
        
        log=${log_file##*/};
        id=${log%%.*};
        n=$id;
        
        for m in $media; do
            output_dir="v2.10.0_sim_"$id"/experiments/"$cult"_mono-culture/"$m;
            
            new_n=$(wc -l $output_dir"/species_trajectory.tsv" | cut -d " " -f1);
            
            n=$n"\t"$new_n;
        done;
        
        echo -e $n >> $output;
    done;    
done

####################### 
# Preliminary analysis

#####
# Extract final point data
log_dir="./log"

cultures="batch continuous"

media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

header=species_ID"\t"mode
for m in $media; do
    header=$header"\t"$m;
done

for cult in $cultures; do
    time_file="./final_point."$cult"_mono-culture.time.tsv";
    od_file="./final_point."$cult"_mono-culture.od.tsv";
    echo -e $header > $time_file;
    echo -e $header > $od_file;
    
    for log_file in $log_dir/*.log; do
        
        log=${log_file##*/};
        simID=${log%%.*};
        speciesID=${simID%_*};
        mode=${simID##*_};
        
        time=$speciesID"\t"$mode;
        od=$speciesID"\t"$mode;
        
        for m in $media; do
            pattern=$cult"_mono-culture with "$m" diet";
            
            add_time=$(grep -A4 "$pattern" $log_file | awk '{FS=" = "} NR==3 {gsub("h","",$2); print $2}');
            time=$time"\t"$add_time;
            
            add_od=$(grep -A4 "$pattern" $log_file | awk '{FS=";"} NR==4 {print $1}' | awk '{FS="="} {print $3}');
            od=$od"\t"$add_od;
        done;
        
        echo -e $time >> $time_file;
        echo -e $od >> $od_file;
            
    done;
done

# Inspecting time files: all batch cultures reached showed no further growth before 42h, except for setA_16 in high_fat and unhealthy. Manually inspecting the growth curves of setA_16 cases showed stationary phase has reached, perhaps due to very slight fluctuation in growth rates, which can be negligible.
# Almost all continuous cultures reached end point 42h. => Require manual inspection. Except for setA_13 in all diets, reached stationary phase before ~4h

####################### 
# Backup data to personal machine: log files; SLURM log files; simulation seeds and loaded_parameters; and all summary files in TSV format
# Run this on personal machine
# $PEREGRINE_MICOM_EXP is the cluster IP address + path to experiments folder on the cluster

ssh-agent bash
ssh-add

parent=$PEREGRINE_MICOM_EXP"setA_mono_simTime42h"

scp $parent"/"*".tsv" .
scp -r $parent"/log/" .
scp -r $parent"/slurm_log/" .

log_dir="./log"

for log_file in $log_dir/*.log; do
    log=${log_file##*/};
    simID=${log%%.*};
    
    sim_dir="v2.10.0_sim_"$simID;
    mkdir -p $sim_dir"/experiments";
    
    scp -r $parent"/"$sim_dir"/experiments/seeds/" $sim_dir"/experiments";
    
    scp $parent"/"$sim_dir"/loaded_parameters.txt" $sim_dir;
    
    scp $parent"/"$sim_dir"/hardcoded_parameters.tsv" $sim_dir;
    
done
