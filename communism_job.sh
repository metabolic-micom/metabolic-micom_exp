#!/bin/bash
#SBATCH --job-name=Communism
#SBATCH --output=./slurm_log/%j.log

#SBATCH --time=02-00:00:00
#SBATCH --partition=regular

#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=4GB

#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=t.s.n.mai@student.rug.nl

# Check if arguments are given
if [ "$#" -eq "0" ]; then
	echo "ERROR: No arguments supplied. Please provide the program in specific version, parameter file name and log file name...";
	exit 1;
fi

if [ -z "$2" ]; then
	echo "ERROR: argument 2 is an empty string. Please provide parameter file name...";
	exit 2;
fi

if [ -z "$3" ]; then
	echo "ERROR: argument 3 is an empty string. Please provide log file name...";
	exit 2;
fi

# Optional 4th argument. This argument is the 2nd argument to the simulation program, which is to specify culture type (batch or continuous) for CO-CULTURE simulations
# If not specified, and CO-CULTURE simulation(s) is run, then it will be in batch culture (default)
# Otherwise, it should be specified as "coculture_batch" or "coculture_continuous"
if [ ! -z "$4" ] && [ "$4" != "coculture_batch" ] && [ "$4" != "coculture_continuous" ]; then
	echo "ERROR: argument 4 should only be either \"coculture_batch\" or \"coculture_continuous\" to specify culture type (batch or continuous) for CO-CULTURE simulations.";
	exit 2;
fi

# Load modules (These have been automatically loaded. Check ~/.bashrc)
#module load R/3.6.1-foss-2018a
#module load cairo/1.14.12-GCCcore-7.3.0
#module load libSBML/5.17.0-GCCcore-7.3.0
#module load GLPK/4.65-GCCcore-7.3.0

# Parse arguments
program=$1
param_file=$2
log_file=$3

# Write JobID
echo -e $SLURM_JOB_ID"\t"$param_file"\t"$log_file >> ./jobs.tsv

# Add job to job-queue
if [ ! -z "$4" ]; then
    coculture_type=$4;
    p=$5; # for perturbation experiments
    $program $param_file $coculture_type $p > $log_file 2>&1;
else
    $program $param_file > $log_file 2>&1;
fi

