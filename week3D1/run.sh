# Herein, $COMMUNISM is an environment variable storing the full path of the program.
# To use this script in your machine, create one such variable or add a line in this script to create a local variable storing the program's location

# Use program version 2.09.1, on branch "nonplasD1"
program_location=${COMMUNISM%/*}
cd $program_location
git checkout nonplasD1
cd -

# Activate environment with R=3.5.1
conda activate micom

# Experiments
# 1. Monoculture test: Lactobacillus_gastricus_PS3 + EU_avg
$COMMUNISM ./parameters/param_w3D1_1.txt > ./w3D1_1.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

# 2. Monoculture test: Bifidobacterium_longum_E18 + EU_avg
$COMMUNISM ./parameters/param_w3D1_2.txt > ./w3D1_2.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

# 3. Coculture test: [Lactobacillus_gastricus_PS3; Bifidobacterium_longum_E18] + EU_avg
$COMMUNISM ./parameters/param_w3D1_3.txt > ./w3D1_3.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

#######################
# Use program version 2.09.1, on branch "main" => Plasticity is allowed
program_location=${COMMUNISM%/*}
cd $program_location
git checkout main
cd -

# 1plastic. Monoculture test: Lactobacillus_gastricus_PS3 + EU_avg
$COMMUNISM ./parameters/param_w3D1_1plastic.txt > ./w3D1_1plastic.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

# 2plastic. Monoculture test: Bifidobacterium_longum_E18 + EU_avg
$COMMUNISM ./parameters/param_w3D1_2plastic.txt > ./w3D1_2plastic.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

# 3plastic. Coculture test: [Lactobacillus_gastricus_PS3; Bifidobacterium_longum_E18] + EU_avg
$COMMUNISM ./parameters/param_w3D1_3plastic.txt > ./w3D1_3plastic.$(date '+%d%m%Y_%H:%M:%S').log 2>&1
