#######################
#######################
# Apr 06, 2021
# Coculture simulations
# List of selected species was manually created
# Species were selected & classified based on "ecological property" of how much the effect of plasticity is in different mono-culture conditions
# simulationTime = 12.0
# program version: v2.10.0
#######################
####################### 

# Arrays of selected species
species_list="./selected_species_setA.tsv"

mapfile -t ecotype_arr < <(tail -n +2 $species_list | cut -f1)
mapfile -t ecotypeID_arr < <(tail -n +2 $species_list | cut -f2)
mapfile -t speciesID_arr < <(tail -n +2 $species_list | cut -f3)
mapfile -t species_arr < <(tail -n +2 $species_list | cut -f4)

#######################
# Prepare parameter files
mkdir parameters/

experiments_dir=$MICOM_EXP # on Peregrine

species_list="./selected_species_setA.tsv"
param_dir="./parameters/"
media_dir=$experiments_dir"materials/media"
sbml_dir=$experiments_dir"materials/sbml_files/agora_1.03"

# EU_avg should always be the first media, because we're using calibration files in EU_avg
media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

mode_list="plastic sfm"        

N=${#species_arr[@]} # number of selected species

for (( i=0; i<$N; i++ )) do
    s=$((i+1));
    
    for (( j=$s; j<$N; j++ )) do
        
        A=${species_arr[$i]};
        A_ecotypeID=${ecotypeID_arr[$i]};
        A_speciesID=${speciesID_arr[$i]};
        
        B=${species_arr[$j]};
        B_ecotypeID=${ecotypeID_arr[$j]};
        B_speciesID=${speciesID_arr[$j]};
        
        echo "Making parameter files for pair "$A_ecotypeID"_"$B_ecotypeID" or "$A_speciesID"_"$B_speciesID" in all media...";
        
        for mode in $mode_list; do
            simID=$A_ecotypeID"_"$B_ecotypeID"_"$mode;
            param_file=$param_dir"/param_"$simID".txt";
        
            echo $simID > $param_file;
        
            for m in $media; do
    
                echo $media_dir"/"$m".csv" >> $param_file;
            
            done;
        
            sbmls=$sbml_dir"/"$A".xml\n"$sbml_dir"/"$B".xml";
            echo -e $sbmls >> $param_file;
        done;
    done;
done

#######################
# Prepare calibration files
# Copy from `experiments/calib_setA`. Use calibration in EU_avg
# Change FLX and YLD of all species to the same desired values
mkdir v2.10.0_growth_parameters_EU_avg/

# Check desired FLX and YLD values, using R script
#conda activate micom # Activate environment with R=3.5.1
../calib_setA/find_FLX_YLD.R

# Change FLX to chosen `FLX` = maximum `FLX` calibrated among all diets and all species = 2246.13
# and Change YLD to chosen `YLD` = value corresponding to the chosen `FLX` = 0.0837152. 
# <=> `FLX` and `YLD` of the most struggling species in its most difficult environment (difficulty = how high biomass production rate can be).

chosen_flx=2246.13;
chosen_yld=0.0837152;

for species in ${species_arr[@]}; do
        
    echo "Preparing calibration directory for species: "$species" ...";
    
    calib_dir="v2.10.0_growth_parameters_EU_avg/"$species;
    cp -r "../calib_setA/"$calib_dir "./"$calib_dir;
    
    echo "Changing FLX and YLD";
    
    calib_file="./"$calib_dir"/growth_curve_parameters.tsv";
        
    awk -v flx=$chosen_flx -v yld=$chosen_yld '{IFS="\t"; OFS="\t"} {if ($1=="FLX") print $1,flx; else if ($1=="YLD") print $1,yld; else print $1,$2}' $calib_file > tmp.$species.tsv;
    
    mv tmp.$species.tsv $calib_file;
        
done

# Checking
head -n5 "./v2.10.0_growth_parameters_EU_avg/"*"/growth_curve_parameters.tsv"

####################### 
# Running & Replicating (on Peregrine cluster)
# 1 job for 1 parameter file - coculture of 1 pair in all media - in either batch or continuous culture
# regular partition, 1 node, 1 task, 1 CPU/task, total mem = 4GB
# Herein, $COMMUNISM_JOB is an environment variable storing full path to the jobscript `communism_job.sh`

param_dir=$PWD"/parameters"
calib_dir=$PWD"/v2.10.0_growth_parameters_EU_avg"

coculture_type_arr=("coculture_batch" "coculture_continuous")

# Testing
# ./culturing $param_dir"/param_I_a_I_b_sfm.txt" $calib_dir sfm rep1 "coculture_batch"

rep_name_list="rep1"
run_moment=$(date '+%d%m%Y_%H:%M:%S') # 13042021_18:01:43

coculture_type=${coculture_type_arr[0]}
for rep_name in $rep_name_list; do
#     for param_file in $param_dir"/param_setA_"*$mode".txt"; do
    for param_file in $param_dir"/param_"*".txt"; do # run for all selected species of set A and in both modes plastic & sfm
        
        param_filename=${param_file%.*};
        mode=${param_filename##*_};
        
        ./culturing $param_file $calib_dir $mode $rep_name $coculture_type;
        echo -e "\n============================================\n";

    done;
    
done > run.$run_moment.log 2>&1

coculture_type=${coculture_type_arr[1]}
for rep_name in $rep_name_list; do
#     for param_file in $param_dir"/param_setA_"*$mode".txt"; do
    for param_file in $param_dir"/param_"*".txt"; do # run for all selected species of set A and in both modes plastic & sfm
        
        param_filename=${param_file%.*};
        mode=${param_filename##*_};
        
        ./culturing $param_file $calib_dir $mode $rep_name $coculture_type;
        echo -e "\n============================================\n";

    done;
    
done >> run.$run_moment.log 2>&1

#######################
# Check hardcoded_parameters
rep_name_list="rep1 rep2 rep3"

echo -e rep_name"\t"simID"\t"flowRate"\t"NFluxModes"\t"fluxNGAM"\t"SwitchRate"\t"SwitchSensitivity"\t"numReps"\t"simTime"(h)\t""initDens(a.u.)" > hardcoded_parameters.tsv

for rep_name in $rep_name_list; do
    
    log_dir="./"$rep_name"/log";
    
    for log_file in $log_dir"/"*".log"; do
        log_filename=${log_file##*/};
        simID=${log_filename%%.*};

        sim_dir="./"$rep_name"/v2.10.0_sim_"$simID;
        data=$(tail -n +2 $sim_dir"/hardcoded_parameters.tsv" | sed -e 's/\t/\\t/g');
        
        echo -e $rep_name"\t"$simID"\t"$data >> hardcoded_parameters.tsv;
    done; 
done

#Check
grep 'plastic' hardcoded_parameters.tsv | cut -f4 | sort | uniq
grep 'sfm' hardcoded_parameters.tsv | cut -f4 | sort | uniq
cut -f9 hardcoded_parameters.tsv | sort | uniq

#######################
# Check running status
rep_name_list="rep1 rep2 rep3"

echo -e rep_name"\t"simID"\t"culture_type"\t"program_log_doneSim"\t"program_log_doneR"\t"slurm_log"\t"run_time"\t"max_mem_used > run_status.tsv

for rep_name in $rep_name_list; do
    
    log_dir="./"$rep_name"/log";
    slurm_dir="./"$rep_name"/slurm_log";
    job_file="./"$rep_name"/jobs.tsv";
    
    for log_file in $log_dir"/"*".log"; do
    
        log_filename=${log_file##*/};
        # separate elements of `log_filename` into an array, elements separated by "."
        IFS="." read -r -a log_filename_arr <<< "$log_filename"
        simID=${log_filename_arr[0]};
        culture_type=${log_filename_arr[1]};
        
        program_log_doneSim=$(grep -c "*** All simulations finished ***" $log_file)
        
        check=$(tail -n 4 $log_file | head -n 1);
        if [[ $check == "*** Finished calling R scripts ***" ]]; then
            program_log_doneR=1;
        else
            program_log_doneR=0;
        fi;
        
        slurm_id=$(grep "log/${log_filename}" $job_file | cut -f1);
        slurm_log_file=$slurm_dir"/"$slurm_id".log";
        slurm_log=$(grep "State" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
        
        run_time=$(grep "Used walltime" $slurm_log_file | cut -d ":" --output-delimiter ":" -f2,3,4 | tr -d " ");
        
        max_mem_used=$(grep "Max Mem used" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
        
        echo -e $rep_name"\t"$simID"\t"$culture_type"\t"$program_log_doneSim"\t"$program_log_doneR"\t"$slurm_log"\t"$run_time"\t"$max_mem_used >> run_status.tsv;
    
    done;
done
