#######################
#######################
# May 03, 2021
# This script stores functions for extracting features from simulation output. 
#######################
#######################

#######################
# Produce ... that contains 
# Take input as 
# Example: ``` ... ```

function finalOD(){
    local culture_type=$1;
    local mode=$2;
    local rep_name_arr=($3);
    local media_arr=($4);
    local members_arr=($5);
    
    if [ -z $6 ]; then
        local output_dir=$PWD;
    else
        local output_dir=$6;
    fi;
    
    if [ -z $7 ]; then
        local species_meta_file=$PWD"/selected_species_setA.tsv";
    else
        local species_meta_file=$7;
    fi;

    header=rep_name"\t"media"\t"ecotype"\t"ecotype_ID"\t"species_ID"\t"species_name;
    for mem in ${members_arr[@]}; do
        header=$header"\t"$mem;
    done;

    local od_file=$output_dir"/final_point."$culture_type"_coculture."$mode".od.tsv";
    local alive_file=$output_dir"/final_point."$culture_type"_coculture."$mode".p_alive.tsv";

    echo -e $header > $od_file;
    echo -e $header > $alive_file;
    
    local N=${#members_arr[@]};
    
    for rep_name in ${rep_name_arr[@]}; do
        for m in ${media_arr[@]}; do
            for (( i=0; i<$N; i++ )) do
                
                local A_ecotypeID=${members_arr[$i]};
                local A=$(grep -w "$A_ecotypeID" $species_meta_file | sed 's/\t/\\t/g');
                local A_name=$(grep -w "$A_ecotypeID" $species_meta_file | cut -f4);
                
                local od=$rep_name"\t"$m"\t"$A;
                local alive=$rep_name"\t"$m"\t"$A;
                
                for (( j=0; j<$N; j++ )) do
                
                    if [ $j -eq $i ]; then
                        local add_od="NA";
                        local add_alive="NA";
                    else
                        local B_ecotypeID=${members_arr[$j]};
                        
                        if [ $j -gt $i ]; then
                            local simID=$A_ecotypeID"_"$B_ecotypeID"_"$mode;
                        else
                            local simID=$B_ecotypeID"_"$A_ecotypeID"_"$mode;
                        fi;
                        
                        datafile="./"$rep_name"/v2.10.0_sim_"$simID"/experiments/"$culture_type"_coculture/"$m"/species_trajectory.tsv";
                    
                        local add_od=$(tail -n2 $datafile | grep $A_name | cut -f6);
                        
                        local add_alive=$(tail -n2 $datafile | grep $A_name | cut -f7);
                    fi;
                    
                    local od=$od"\t"$add_od;
                    local alive=$alive"\t"$add_alive;
                done;
                
                echo -e $od >> $od_file;
                echo -e $alive >> $alive_file;
            done;            
        done;
    done;
} 
