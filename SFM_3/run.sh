# Herein, $COMMUNISM is an environment variable storing the full path of the program.
# To use this script in your machine, create one such variable or add a line in this script to create a local variable storing the program's location

# Activate environment with R=3.5.1
conda activate micom

#######################
# Use program version 2.09.1, on branch "SFM_3plastic" => Plasticity is allowed, 20 flux modes
program_location=${COMMUNISM%/*}
cd $program_location
git checkout SFM_3plastic
cd -

# 1plastic. Monoculture test: Bifidobacterium_longum_E18 + EU_avg
$COMMUNISM ./parameters/param_sfm3_1plastic.txt > ./sfm3_1plastic.$(date '+%d%m%Y_%H:%M:%S').log 2>&1
    
#######################
# Use program version 2.09.1, on branch "SFM_3" <=> Same codes as in "SFM_3plastic" but single flux mode
program_location=${COMMUNISM%/*}
cd $program_location
git checkout SFM_3
cd -
    
# Experiments
# 1. Monoculture test: Bifidobacterium_longum_E18 + EU_avg
$COMMUNISM ./parameters/param_sfm3_1.txt > ./sfm3_1.$(date '+%d%m%Y_%H:%M:%S').log 2>&1
