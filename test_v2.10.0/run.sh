# Herein, $COMMUNISM is an environment variable storing the full path of the program.
# To use this script in your machine, create one such variable or add a line in this script to create a local variable storing the program's location

# Activate environment with R=3.5.1
conda activate micom

#######################
# Testing program version 2.10.0
# Monoculture: Corynebacterium_ulcerans_809 in EU_avg
#######################

#######################
# Use program version 2.10.0, on branch "v2.10.0_calib" <=> CALIBRATION-ONLY mode
program_location=${COMMUNISM%/*}
cd $program_location
git checkout v2.10.0_calib
cd -

$COMMUNISM ./parameters/param_test_calib.txt > ./test_calib.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

#######################
# Use program version 2.10.0, on branch "v2.10.0_plastic" <=> Plasticity mode with 20 flux modes
program_location=${COMMUNISM%/*}
cd $program_location
git checkout v2.10.0_plastic
cd -

$COMMUNISM ./parameters/param_test_plastic.txt > ./test_plastic.$(date '+%d%m%Y_%H:%M:%S').log 2>&1
    
#######################
# Use program version 2.10.0, on branch "v2.10.0_sfm" <=> SFM mode with 1 flux mode
program_location=${COMMUNISM%/*}
cd $program_location
git checkout v2.10.0_sfm
cd -

$COMMUNISM ./parameters/param_test_sfm.txt > ./test_sfm.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

#######################
#######################
# Apr 06, 2021
# Test coculture
# setA_16 and setA_21
# in EU_avg
#######################
####################### 

simID="coculture_A_16-21_plastic"
simID="coculture_A_16-21_sfm"

m="EU_avg"
A="Lactobacillus_gastricus_PS3" #setA_16
B="Pseudomonas_nitroreducens_HBP1" #seA_21
species_list=($A $B)
param_file="./parameters/param_"$simID".txt"

####################### 
# Prepare parameter file
experiments_dir=$MICOM"experiments/" # on T470
media_dir=$experiments_dir"materials/media"
sbml_dir=$experiments_dir"materials/sbml_files/agora_1.03"

echo $simID > $param_file
echo $media_dir"/"$m".csv" >> $param_file
echo $sbml_dir"/"$A".xml" >> $param_file
echo $sbml_dir"/"$B".xml" >> $param_file

####################### 
# Prepare calibration folder
chosen_flx=2246.13;
chosen_yld=0.0837152;

for species in ${species_list[@]}; do

    calib_dirname="v2.10.0_growth_parameters_EU_avg";
    mkdir -p $calib_dirname;
    
    cp -r "../calib_setA/"$calib_dirname"/"$species "./"$calib_dirname;
    
    calib_dir="./v2.10.0_growth_parameters_EU_avg/"$species;
    calib_file=$calib_dir"/growth_curve_parameters.tsv";
    
    awk -v flx=$chosen_flx -v yld=$chosen_yld '{IFS="\t"; OFS="\t"} {if ($1=="FLX") print $1,flx; else if ($1=="YLD") print $1,yld; else print $1,$2}' $calib_file > tmp.$species.tsv;
    
    mv tmp.$species.tsv $calib_file;
    
done

# Checking
head -n5 "./v2.10.0_growth_parameters_EU_avg/"*"/growth_curve_parameters.tsv"

####################### 
conda activate micom

# Remember to check which plasticity mode is using!!!
$COMMUNISM $param_file > ./$simID.batch.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

$COMMUNISM $param_file coculture_continuous > ./$simID.continuous.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

####################### 
# Use program version 2.10.0, on branch "v2.10.0_sfm.perturbed" <=> SFM mode with 1 flux mode, persistence experiment
$COMMUNISM $param_file coculture_continuous 0 > ./$simID.continuous_0.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

$COMMUNISM $param_file coculture_continuous 1 > ./$simID.continuous_1.$(date '+%d%m%Y_%H:%M:%S').log 2>&1
