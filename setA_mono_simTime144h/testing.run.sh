#######################
#######################
# Run plastic (~~and non-plastic~~) monoculture experiments on set A, tested in all diets
# Use program version 2.10.0, on branch "v2.10.0_plastic" and branch "v2.10.0_sfm"
# To save less data points, in Environment::simulateTrajectory(...), always output=false
# Simulation time extended to 144h for species to reach stationary phase
# `dtMax = delta * 24.0` => Obtain at least 1000 data points per 24h
# Running R scripts for visualization was turned off
#######################
#######################


#######################
#######################
# Mar 10, 2021
# Testing on some species that have been found to not reach stationary phase at 42h
# Use parameter + calib + simulation seeds from `setA_mono_simTime42h`
simIDs="setA_15_plastic setA_18_plastic setA_19_plastic setA_3_plastic setA_4_plastic setA_6_plastic setA_8_plastic setA_22_plastic"

parent="../setA_mono_simTime42h"

cp -r $parent"/v2.10.0_growth_parameters_EU_avg/" .

mkdir ./parameters
param_dir="./parameters"

for id in $simIDs; do
    cp $parent"/parameters/param_"$id".txt" $param_dir;
    
    sim_dir="v2.10.0_sim_"$id;
    mkdir -p $sim_dir"/experiments/";
    cp -r $parent"/"$sim_dir"/experiments/seeds/" $sim_dir"/experiments/";
    cp $parent"/"$sim_dir"/loaded_parameters.txt" $sim_dir;
done

####################### 
# Running (on Peregrine cluster)
# 1 job for 1 species in 1 environment
# regular partition, 1 node, 1 task, 1 CPU/task
# Herein, $COMMUNISM_JOB is an environment variable storing full path to the jobscript `communism_job.sh`

mkdir ./slurm_log/
mkdir ./log/

param_dir="./parameters"
log_dir="./log"

mode="plastic"
program="/home/s4278836/First_project/v2.10.0_plastic_144h/communism"

now="rep1"
for param_file in $param_dir"/param_setA_"*$mode".txt"; do
        
    echo $param_file;
    simIDtxt=${param_file#*_};
    simID=${simIDtxt%.*};
                
    log_file=$log_dir"/"$simID"."$now".log";
        
    sbatch $COMMUNISM_JOB $program $param_file $log_file; # 1 CPU/task + total mem = 4GB. 
    
done

#####
# Check if all have run completely
slurm_dir="./slurm_log"
log_dir="./log"

mode="plastic"
now="rep1"

echo -e ID"\t"program_log_doneSim"\t"program_log_doneR"\t"slurm_log"\t"run_time"\t"max_mem_used > $now.status.tsv

for log_file in $log_dir"/setA_"*$mode"."$now".log"; do
    
    log=${log_file##*/};
    id=${log%%.*};
    
    program_log_doneSim=$(grep -c "*** All simulations finished ***" $log_file)
    
    check=$(tail -n 4 $log_file | head -n 1);
    if [[ $check == "*** Finished calling R scripts ***" ]]; then
        program_log_doneR=1;
    else
        program_log_doneR=0;
    fi;
    
    slurm_id=$(grep "$log_file" jobs.tsv | cut -f1);
    slurm_log_file=$slurm_dir"/"$slurm_id".log";
    slurm_log=$(grep "State" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
    
    run_time=$(grep "Used walltime" $slurm_log_file | cut -d ":" --output-delimiter ":" -f2,3,4 | tr -d " ");
    
    max_mem_used=$(grep "Max Mem used" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
    
    echo -e $id"\t"$program_log_doneSim"\t"$program_log_doneR"\t"$slurm_log"\t"$run_time"\t"$max_mem_used >> $now.status.tsv;
                       
done

#####
# Check hardcoded_parameters
log_dir="./log"

mode="plastic"
now="rep1"

echo -e ID"\t"flowRate"\t"NFluxModes"\t"fluxNGAM"\t"SwitchRate"\t"SwitchSensitivity"\t"numReps"\t"simTime"(h)\t""initDens(a.u.)" > $now.hardcoded_parameters.tsv

for log_file in $log_dir"/setA_"*$mode"."$now".log"; do
    log=${log_file##*/};
    id=${log%%.*}
    
    sim_dir="v2.10.0_sim_"$id;
    data=$(tail -n +2 $sim_dir"/hardcoded_parameters.tsv" | sed -e 's/\t/\\t/g');
    
    echo -e $id"\t"$data >> $now.hardcoded_parameters.tsv;
done

####################### 
# Check number of data points
log_dir="./log"

cultures="batch continuous"

media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

header="ID"
for m in $media; do
    header=$header"\t"$m;
done

for cult in $cultures; do
    output="data_points."$cult"_mono-culture.tsv";
    echo -e $header > $output;

    for log_file in $log_dir/*.log; do
        
        log=${log_file##*/};
        id=${log%%.*};
        n=$id;
        
        for m in $media; do
            output_dir="v2.10.0_sim_"$id"/experiments/"$cult"_mono-culture/"$m;
            
            new_n=$(wc -l $output_dir"/species_trajectory.tsv" | cut -d " " -f1);
            
            n=$n"\t"$new_n;
        done;
        
        echo -e $n >> $output;
    done;    
done

####################### 
# Extract final point data
log_dir="./log"

cultures="batch continuous"

media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

header=species_ID"\t"mode
for m in $media; do
    header=$header"\t"$m;
done

for cult in $cultures; do
    time_file="./final_point."$cult"_mono-culture.time.tsv";
    od_file="./final_point."$cult"_mono-culture.od.tsv";
    echo -e $header > $time_file;
    echo -e $header > $od_file;
    
    for log_file in $log_dir/*.log; do
        
        log=${log_file##*/};
        simID=${log%%.*};
        speciesID=${simID%_*};
        mode=${simID##*_};
        
        time=$speciesID"\t"$mode;
        od=$speciesID"\t"$mode;
        
        for m in $media; do
            pattern=$cult"_mono-culture with "$m" diet";
            
            add_time=$(grep -A4 "$pattern" $log_file | awk '{FS=" = "} NR==3 {gsub("h","",$2); print $2}');
            time=$time"\t"$add_time;
            
            add_od=$(grep -A4 "$pattern" $log_file | awk '{FS=";"} NR==4 {print $1}' | awk '{FS="="} {print $3}');
            od=$od"\t"$add_od;
        done;
        
        echo -e $time >> $time_file;
        echo -e $od >> $od_file;
            
    done;
done

# All continuous culture run until 144h and have not been stopped by the program
#######################
#######################
