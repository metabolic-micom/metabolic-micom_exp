#######################
#######################
# Mar 14, 2021
# Produce 3 replicates for all 18 species of set A | monoculture | plastic + sfm
# For the replicate 1, PLASTIC simulations re-use random seeds from `setA_mono_simTime42h`
# Program version: v2.10.0 modified for personal use to sample at least 1000 data points per 24h and does not sample data when there is a rapid change in bacteria density
# There are 3 sub-directories for 3 replicates. In each of these sub-directory, there are simulation ouput directories for each species in each plasticity mode. There is also directory of calibration parameters, which is copied from the main directory
# The main directory contains parameters directory, calibration directory, scripts (including this one), and output files of preliminary analyses.
#######################
#######################

#######################
# Get list of species
# Select only species that can be calibrated
# As has been observed in `calib_setA`, once a species cannot be calibrated in one environment, it also cannot be calibrated in all other environments
# Pick calib folder in EU_avg as the representative `calib_dir`
large_species_list="../selected_species_sets.tsv"
species_list="./selected_species_setA.tsv"

echo -e Species_ID"\t"Species_name > $species_list

calib_dir="../calib_setA/v2.10.0_growth_parameters_EU_avg"

while IFS=$'\t' read -r set species_numb species; do #run through every species
    if [[ $set == "A" ]]; then
        if [ -d $calib_dir"/"$species ]; then #check if directory for calibration parameters exists
            echo -e "setA_"$species_numb"\t"$species >> $species_list;
        fi;
    fi;
done < <(tail -n +2 $large_species_list | cut -f1,2,3)

#######################
# Prepare parameter files
mkdir parameters/

experiments_dir=$MICOM_EXP # on Peregrine

species_list="./selected_species_setA.tsv"
param_dir="./parameters/"
media_dir=$experiments_dir"materials/media"
sbml_dir=$experiments_dir"materials/sbml_files/agora_1.03"

# EU_avg should always be the first media, because we're using calibration files in EU_avg
media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

mode_list="plastic sfm"        

while IFS=$'\t' read -r speciesID species; do
        
        echo "Making parameter files for species: "$speciesID" in all media...";
        
        for mode in $mode_list; do
            simID=$speciesID"_"$mode;
            param_file=$param_dir"/param_"$simID".txt";
        
            echo $simID > $param_file;
        
            for m in $media; do
    
                echo $media_dir"/"$m".csv" >> $param_file;
            
            done;
        
            sbml=$sbml_dir"/"$species".xml";
            echo $sbml >> $param_file;
        done;
        
done < <(tail -n +2 $species_list | cut -f1,2,3)

#######################
# Prepare calibration files
# Copy from `experiments/calib_setA`. Use calibration in EU_avg
# Change FLX and YLD of all species to the same desired values
cp -r ../calib_setA/v2.10.0_growth_parameters_EU_avg/ .

# Check desired FLX and YLD values, using R script

#conda activate micom # Activate environment with R=3.5.1
../calib_setA/find_FLX_YLD.R

# Change FLX to chosen `FLX` = maximum `FLX` calibrated among all diets and all species = 2246.13
# and Change YLD to chosen `YLD` = value corresponding to the chosen `FLX` = 0.0837152. 
# <=> `FLX` and `YLD` of the most struggling species in its most difficult environment (difficulty = how high biomass production rate can be).
species_list="./selected_species_setA.tsv"
chosen_flx=2246.13;
chosen_yld=0.0837152;

while IFS=$'\t' read -r speciesID species; do
        
        echo "Changing FLX and YLD for species: "$speciesID" ...";
        
        calib_dir="./v2.10.0_growth_parameters_EU_avg/"$species;
        calib_file=$calib_dir"/growth_curve_parameters.tsv";
            
        awk -v flx=$chosen_flx -v yld=$chosen_yld '{IFS="\t"; OFS="\t"} {if ($1=="FLX") print $1,flx; else if ($1=="YLD") print $1,yld; else print $1,$2}' $calib_file > tmp.$speciesID.tsv;
        
        mv tmp.$speciesID.tsv $calib_file;
        
done < <(tail -n +2 $species_list | cut -f1,2,3)

# Checking
head -n5 "./v2.10.0_growth_parameters_EU_avg/"*"/growth_curve_parameters.tsv"

####################### 
# Copy random seeds for PLASTIC simulations of Replicate 1
rep_name="rep1"
mkdir ./$rep_name/

parent="../setA_mono_simTime42h"

param_dir="./parameters"

for param_file in $param_dir"/"*"_plastic.txt"; do
    
    filename=${param_file%.*};
    simID=${filename#*_};
    
    sim_dir="v2.10.0_sim_"$simID;
    
    mkdir -p "./"$rep_name"/"$sim_dir"/experiments/";
    cp -r $parent"/"$sim_dir"/experiments/seeds/" "./"$rep_name"/"$sim_dir"/experiments/";
    tail -n +2 $param_file > "./"$rep_name"/"$sim_dir"/loaded_parameters.txt";

done

# Checking
ls $rep_name
ls $rep_name | wc -l

ls $rep_name/*
cat $rep_name/*/loaded_parameters.txt

ls $rep_name/*/experiments/
ls $rep_name/*/experiments/seeds

####################### 
# Running & Replicating (on Peregrine cluster)
# 1 job for 1 species in all environments
# regular partition, 1 node, 1 task, 1 CPU/task, total mem = 4GB
# Herein, $COMMUNISM_JOB is an environment variable storing full path to the jobscript `communism_job.sh`

export -f monoculture
monoculture(){
    # Parsing arguments
    param_file=$1;
    echo "Parameter file: "$param_file;
    calib_dir=$2;
    echo "Calibration directory: "$calib_dir;
    mode=$3;
    echo "Mode: "$mode;
    if [[ $4 != "" ]]; then        
        rep_name_avail=true;
        rep_name=$4;
        working_dir="./"$rep_name;
    else
        rep_name_avail=false;
        working_dir="./";
    fi;
    echo "Working directory: "$working_dir;
    
    # Choose program sub-version according to the specified mode
    program="COMMUNISM_2100_"${mode^^};
    echo "Program - environment variable name: "$program;
    echo "Program - absolute path: "${!program};
    
    # Set up directory
    log_dir="log";
    mkdir -p $working_dir"/"$log_dir;
    mkdir -p $working_dir"/slurm_log";
    cp -r $calib_dir $working_dir;
    
    # Get simulation ID
    simID=$(head -n1 $param_file);
    echo "Simulation ID: "$simID;
    
    # Define filename of log file
    if $rep_name_avail; then
        log_file=$log_dir"/"$simID"."$rep_name".log";
    else
        now=$(date '+%d%m%Y_%H:%M:%S');
        log_file=$log_dir"/"$simID"."$now".log";
    fi;
    echo "Log file: "$working_dir"/"$log_file;
    
    # Go to working directory and submit job from there
    (
        cd $working_dir;
        sbatch $COMMUNISM_JOB ${!program} $param_file $log_file;
    );
    
}

param_dir=$PWD"/parameters"
calib_dir=$PWD"/v2.10.0_growth_parameters_EU_avg"

rep_name_list="rep1"
run_moment=$(date '+%d%m%Y_%H:%M:%S') # 15032021_11:29:34

rep_name_list="rep2 rep3"
run_moment=$(date '+%d%m%Y_%H:%M:%S') # 15032021_17:55:13

rep_name_list="rep1 rep2 rep3"
run_moment=$(date '+%d%m%Y_%H:%M:%S') # 22032021_23:00:05

rep_name_list="rep1 rep2 rep3"
run_moment=$(date '+%d%m%Y_%H:%M:%S') # 07042021_22:23:58

for rep_name in $rep_name_list; do
#     for param_file in $param_dir"/param_setA_"*$mode".txt"; do
    for param_file in $param_dir"/param_setA_"*".txt"; do # run for all selected species of set A and in both modes plastic & sfm
        
        param_filename=${param_file%.*};
        mode=${param_filename##*_};
        monoculture $param_file $calib_dir $mode $rep_name;
        echo -e "\n============================================\n";
    done;
    
done > run.$run_moment.log 2>&1

#######################
# Check hardcoded_parameters
rep_name_list="rep1 rep2 rep3"

echo -e rep_name"\t"simID"\t"flowRate"\t"NFluxModes"\t"fluxNGAM"\t"SwitchRate"\t"SwitchSensitivity"\t"numReps"\t"simTime"(h)\t""initDens(a.u.)" > hardcoded_parameters.tsv

for rep_name in $rep_name_list; do
    
    log_dir="./"$rep_name"/log";
    
    for log_file in $log_dir"/setA_"*".log"; do
        log_filename=${log_file##*/};
        simID=${log_filename%%.*};

        sim_dir="./"$rep_name"/v2.10.0_sim_"$simID;
        data=$(tail -n +2 $sim_dir"/hardcoded_parameters.tsv" | sed -e 's/\t/\\t/g');
        
        echo -e $rep_name"\t"$simID"\t"$data >> hardcoded_parameters.tsv;
    done; 
done

#######################
# Check running status
rep_name_list="rep1 rep2 rep3"

echo -e rep_name"\t"simID"\t"program_log_doneSim"\t"program_log_doneR"\t"slurm_log"\t"run_time"\t"max_mem_used > run_status.tsv

for rep_name in $rep_name_list; do
    
    log_dir="./"$rep_name"/log";
    slurm_dir="./"$rep_name"/slurm_log";
    job_file="./"$rep_name"/jobs.tsv";
    
    for log_file in $log_dir"/setA_"*".log"; do
    
        log_filename=${log_file##*/};
        simID=${log_filename%%.*};
        
        program_log_doneSim=$(grep -c "*** All simulations finished ***" $log_file)
        
        check=$(tail -n 4 $log_file | head -n 1);
        if [[ $check == "*** Finished calling R scripts ***" ]]; then
            program_log_doneR=1;
        else
            program_log_doneR=0;
        fi;
        
        slurm_id=$(grep "log/${log_filename}" $job_file | cut -f1);
        slurm_log_file=$slurm_dir"/"$slurm_id".log";
        slurm_log=$(grep "State" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
        
        run_time=$(grep "Used walltime" $slurm_log_file | cut -d ":" --output-delimiter ":" -f2,3,4 | tr -d " ");
        
        max_mem_used=$(grep "Max Mem used" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
        
        echo -e $rep_name"\t"$simID"\t"$program_log_doneSim"\t"$program_log_doneR"\t"$slurm_log"\t"$run_time"\t"$max_mem_used >> run_status.tsv;
    
    done;
done

####################### 
# Preliminary analysis
# Extract final point data

rep_name_list="rep1 rep2 rep3"

cultures="batch continuous"

media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

header=rep_name"\t"species_ID"\t"mode
for m in $media; do
    header=$header"\t"$m;
done
   
for cult in $cultures; do
    time_file="./final_point."$cult"_mono-culture.time.tsv";
    od_file="./final_point."$cult"_mono-culture.od.tsv";
    alive_file="./final_point."$cult"_mono-culture.p_alive.tsv";
    echo -e $header > $time_file;
    echo -e $header > $od_file;
    echo -e $header > $alive_file;
        
    for rep_name in $rep_name_list; do
    
        log_dir="./"$rep_name"/log";
        
        for log_file in $log_dir"/setA_"*".log"; do
            
            log=${log_file##*/};
            simID=${log%%.*};
            speciesID=${simID%_*};
            mode=${simID##*_};
            
            time=$rep_name"\t"$speciesID"\t"$mode;
            od=$rep_name"\t"$speciesID"\t"$mode;
            alive=$rep_name"\t"$speciesID"\t"$mode;
            
            for m in $media; do
                pattern=$cult"_mono-culture with "$m" diet";
                
                add_time=$(grep -A4 "$pattern" $log_file | awk '{FS=" = "} NR==3 {gsub("h","",$2); print $2}');
                time=$time"\t"$add_time;
                
                add_od=$(grep -A4 "$pattern" $log_file | awk '{FS=";"} NR==4 {print $1}' | awk '{FS="="} {print $3}');
                od=$od"\t"$add_od;
                
                datafile="./"$rep_name"/v2.10.0_sim_"$simID"/experiments/"$cult"_mono-culture/"$m"/species_trajectory.tsv";
                add_alive=$(tail -n1 $datafile | cut -f7);
                alive=$alive"\t"$add_alive;
            done;
            
            echo -e $time >> $time_file;
            echo -e $od >> $od_file;
            echo -e $alive >> $alive_file;
                
        done;
    done;
done

# Inspecting time files: all batch cultures reached showed no further growth before 144h, except for setA_16 in high_fat and unhealthy. This is true for both plastic and non-plastic and for all first 3 replicates. 
# Manually inspecting the growth curves of setA_16 cases showed stationary phase has reached, perhaps due to very slight fluctuation in growth rates, which can be negligible.

# Almost all continuous cultures reached end point 144h. => Require manual inspection. Except for setA_13_plastic in all diets, which reached stationary phase before ~4h. This is also true for all first 3 replicates.

# Extract initial growth rate
rep_name_list="rep1 rep2 rep3"

cultures="batch continuous"

media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

header=rep_name"\t"species_ID"\t"mode
for m in $media; do
    header=$header"\t"$m;
done

for cult in $cultures; do
    initr_file="./init_r."$cult"_mono-culture.tsv";
    echo -e $header > $initr_file;

    for rep_name in $rep_name_list; do
    
        log_dir="./"$rep_name"/log";
        
        for log_file in $log_dir"/setA_"*".log"; do
            
            log=${log_file##*/};
            simID=${log%%.*};
            speciesID=${simID%_*};
            mode=${simID##*_};
            
            initr=$rep_name"\t"$speciesID"\t"$mode;
            
            for m in $media; do
                datafile="./"$rep_name"/v2.10.0_sim_"$simID"/experiments/"$cult"_mono-culture/"$m"/species_trajectory.tsv";
                
                add_initr=$(tail -n +2 $datafile | head -n1 | cut -f8);
                initr=$initr"\t"$add_initr;
            done;

            echo -e $initr >> $initr_file;
                
        done;
    done;
done

####################### 
# Backup data to personal machine: log files; SLURM log files; simulation seeds and loaded_parameters; and all summary files in TSV format
# ~~Also backup simulation output: species_trajectory.tsv~~
# Run this on personal machine
# $PEREGRINE_MICOM_EXP is the cluster IP address + path to experiments folder on the cluster

ssh-agent bash
ssh-add

parent=$PEREGRINE_MICOM_EXP"setA_mono_simTime144h"
scp $parent"/"*".tsv" .
scp $parent"/"*".log" .

backup(){
    source=$1;
    dest=$2;
    
    scp $source"/"*".tsv" $dest;
    scp -r $source"/log/" $dest;
    scp -r $source"/slurm_log/" $dest;
    
    log_dir=$dest"/log";

    for log_file in $log_dir/*.log; do
        log_filename=${log_file##*/};
        simID=${log_filename%%.*};
        
        sim_dir="v2.10.0_sim_"$simID;
        mkdir -p $dest"/"$sim_dir"/experiments";
        
        scp -r $source"/"$sim_dir"/experiments/seeds/" $dest"/"$sim_dir"/experiments";
        
        scp $source"/"$sim_dir"/loaded_parameters.txt" $dest"/"$sim_dir;
        
        scp $source"/"$sim_dir"/hardcoded_parameters.tsv" $dest"/"$sim_dir;
        
#         Saving data output
#         media="EU_avg DACH diabetes2 gluten_free \
#             high_fat high_fiber high_protein \
#             mediterranean unhealthy \
#             vegan vegetarian";
#             
#         cultures="batch continuous";
#         
#         for cult in $cultures; do
#             for m in $media; do
#                 
#                 output_dir=$sim_dir"/experiments/"$cult"_mono-culture/"$m;
#                 mkdir -p $dest"/"$output_dir;
#                 
#                 scp $parent"/"$output_dir"/species_trajectory.tsv" $dest"/"$output_dir;
#                 
#             done;
#         done;
    done; 
}

export -f backup

rep_name_list="rep1 rep2 rep3"

for rep_name in $rep_name_list; do
    
    mkdir $rep_name;
    parent=$PEREGRINE_MICOM_EXP"setA_mono_simTime144h/"$rep_name;
    
    backup $parent "./"$rep_name;
    
done 2> backup.err

grep -v 'The following have been reloaded with a version change:' backup.err | grep -v -E ' ?)' | grep -v '^$'

