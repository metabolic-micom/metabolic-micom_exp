# Herein, $COMMUNISM is an environment variable storing the full path of the program.
# To use this script in your machine, create one such variable or add a line in this script to create a local variable storing the program's location

# Activate environment with R=3.5.1
#conda activate micom

#######################
#######################
# Feb 10, 2021
# Try calibrating every species in Set A in every diet
# Use program version 2.10.0, on branch "v2.10.0_calib" <=> CALIBRATION-ONLY mode
#######################
#######################

#######################
#######################
# Activate "v2.10.0_calib"
program_location=${COMMUNISM%/*}
cd $program_location
git checkout v2.10.0_calib
cd -

#######################
#######################
# For every media, calibrate every species in the same set in different runs
# Run parallely across species

#######################
# Prepare parameter files
mkdir parameters/

species_list="../selected_species_sets.tsv"
param_dir="./parameters"
media_dir="../materials/media"
sbml_dir="../materials/sbml_files/agora_1.03"

media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

while IFS=$'\t' read -r set species_numb species; do
    if [[ $set == "A" ]]; then
        for m in $media; do
        
            echo "Making parameter files for species: "$species_numb" in media: "$m;
            
            simID="setA_"$species_numb"_"$m;
            param_file=$param_dir"/param_"$simID".txt";
    
            echo $simID > $param_file;
    
            echo $media_dir"/"$m".csv" >> $param_file;
            
            sbml=$sbml_dir"/"$species".xml";
            echo $sbml >> $param_file;
            
        done;
    fi;
done < <(tail -n +2 $species_list | cut -f1,2,3)

#######################
# Run on personal laptop
mkdir ./log/

param_dir="./parameters"
log_dir="./log"

media="EU_avg"

# Create list of parameter files and corresponding log files 
param_files=""
log_files=""

now=$(date '+%d%m%Y_%H:%M:%S')
for m in $media; do
    for i in {1..27}; do #select all 27 species in set
        
        add_param=$param_dir"/param_setA_"$i"_"$m".txt";
        param_files=$param_files$add_param" ";
        
        add_log=$log_dir"/setA_"$i"_"$m"."$now".log";
        log_files=$log_files$add_log" ";
        
    done;            
done

# Function to run calibration
calib_parallel(){
    param_file=$1;
    log_file=$2;
    echo -e "\nCalibrating: "$param_file" ...";
    $COMMUNISM $param_file > $log_file 2>&1;
}
export -f calib_parallel

# Run parallely
parallel --jobs 3 --link calib_parallel ::: $param_files ::: $log_files

# Check if all have run completely
for file in $log_files; do
    
    log=${file##*/};
    id=${log%%.*};
    
    check=$(tail -n 4 $file | head -n 1);
    if [[ $check == "*** Finished calibrating species growth curve(s) ***" ]]; then
        echo -e $id"\t"1"\n";
    else
        echo -e $id"\t"0"\n";
    fi;
    
done

####################### 
# Run on Peregrine cluster
# 1 job for 1 species in 1 environment
# regular partition, 1 node, 1 task, 1 CPU/task
# Herein, in addition to $COMMUNISM, $COMMUNISM_JOB is an environment variable storing full path to the jobscript `communism_job.sh`
mkdir ./slurm_log/
mkdir ./log/

param_dir="./parameters"
log_dir="./log"

#media="EU_avg" #aleady completed on personal laptop
media="DACH diabetes2 gluten_free" # now="16022021_22:01:58"
media="high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian" #now="16022021_23:15:21"

now=$(date '+%d%m%Y_%H:%M:%S');
for m in $media; do
    for i in {1..27}; do #select all 27 species in set
        
        param_file=$param_dir"/param_setA_"$i"_"$m".txt";
                
        log_file=$log_dir"/setA_"$i"_"$m"."$now".log";
        
        sbatch $COMMUNISM_JOB $COMMUNISM_2100_CALIB $param_file $log_file;
        
    done;            
done

# Check if all have run completely
slurm_dir="./slurm_log"
param_dir="./parameters"
log_dir="./log"

media="DACH diabetes2 gluten_free"
now="16022021_22:01:58"
media="high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"
now="16022021_23:15:21"

echo -e ID"\t"program_log"\t"slurm_log > $now.status

for m in $media; do
    for i in {1..27}; do #select all 27 species in set
                
        log_file=$log_dir"/setA_"$i"_"$m"."$now".log";
        
        log=${log_file##*/};
        id=${log%%.*};
        
        check=$(tail -n 4 $log_file | head -n 1);
        if [[ $check == "*** Finished calibrating species growth curve(s) ***" ]]; then
            program_log=1;
        else
            program_log=0;
        fi;
        
        slurm_id=$(grep "$log_file" jobs.tsv | cut -f1);
        slurm_log_file=$slurm_dir"/"$slurm_id".log";
        slurm_log=$(grep "State" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
        
        echo -e $id"\t"$program_log"\t"$slurm_log >> $now.status;
            
    done;            
done

#######################
# Summary on which species can and cannot be calibrated
# with the condition that all runs were completed without any disruption
species_list="../selected_species_sets.tsv"
media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"
out_file="summary.calibOrNot.tsv"

media_header=""
pre_header="Set\tSpecies_number\tSpecies_name"

while IFS=$'\t' read -r set species_numb species; do #run through every species
    if [[ $set == "A" ]]; then
        
        data=$set"\t"$species_numb"\t"$species;
        
        for m in $media; do 
            
            media_header=$media_header"\t"$m;
            
            calib_dir="v2.10.0_growth_parameters_"$m;
            
            if [ -d $calib_dir"/"$species ]; then #check if directory for calibration parameters exists
                data=$data"\t"1;
            else
                data=$data"\t"0;
            fi;
            
        done;
        
        if [[ $species_numb -eq 1 ]]; then
            echo -e $pre_header$media_header > $out_file;
        fi
        
        echo -e $data >> $out_file;
        
    fi;
done < <(tail -n +2 $species_list | cut -f1,2,3)

# Summary on calib parameters
# with the condition that all runs were completed without any disruption
species_list="../selected_species_sets.tsv"
media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"
out_file="summary.calibParam.tsv"

param_header=""
pre_header="Set\tSpecies_number\tSpecies_name"

while IFS=$'\t' read -r set species_numb species; do #run through every species
    if [[ $set == "A" ]]; then
        
        data=$set"\t"$species_numb"\t"$species;
        
        for m in $media; do 
            
            param_header=$param_header"\tFLX_"$m"\tYLD_"$m"\tCRW_"$m"\tCNC_"$m"\trmax_"$m"\tmaxOD_"$m;
            
            calib_dir="v2.10.0_growth_parameters_"$m;
            
            if [ -d $calib_dir"/"$species ]; then #check if directory for calibration parameters exists
                in_file=$calib_dir"/"$species"/growth_curve_parameters.tsv";
                new_data=$(cut -f2 $in_file | sed -z 's/\n/\\t/g');
            else
                new_data=NA"\t"NA"\t"NA"\t"NA"\t"NA"\t"NA"\t";
            fi;
            
            data=$data"\t"${new_data%\\*}; #add values to data, remove the last tab
            
        done;
        
        if [[ $species_numb -eq 1 ]]; then
            echo -e $pre_header$param_header > $out_file;
        fi
        
        echo -e $data >> $out_file;
        
    fi;
done < <(tail -n +2 $species_list | cut -f1,2,3)

#######################
#######################

#######################
#######################
# For every media, calibrate all species in the same set in one run
# This takes a lot of time, cannot run parallely across species

#######################
# Prepare parameter files
mkdir parameters/

species_list="../selected_species_sets.tsv"
param_dir="./parameters"
media_dir="../materials/media"
sbml_dir="../materials/sbml_files/agora_1.03"

media="EU_avg DACH diabetes2 gluten_free \
        high_fat high_fiber high_protein \
        mediterranean unhealthy \
        vegan vegetarian"

for m in $media; do
    echo "Making parameter file for media: "$m;
    
    simID="setA_"$m;
    out_file=$param_dir"/param_"$simID".txt";
    
    echo $simID > $out_file;
    
    echo $media_dir"/"$m".csv" >> $out_file;
    
    while IFS=$'\t' read -r set species; do
        if [[ $set == "A" ]]; then
            sbml=$sbml_dir"/"$species".xml";
            echo $sbml >> $out_file;
        fi;
    done < <(cat $species_list | cut -f1,2);
    
done

#######################
# Run
mkdir ./log/

param_dir="./parameters"
log_dir="./log"

media="EU_avg DACH"

for m in $media; do
        
    echo "Calibrating set A in media: "$m" ...";
        
    param_file=$param_dir"/param_setA_"$m".txt";
    log_file=$log_dir"/setA_"$m"."$(date '+%d%m%Y_%H:%M:%S')".log";
        
    $COMMUNISM $param_file > $log_file 2>&1;
        
done
#######################
#######################


