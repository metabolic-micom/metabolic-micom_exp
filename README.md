 

# Experiments in project metabolic_micom

There are folder `materials` containing materials for running simulation, and all other folders storing simulation running scripts, running log files, parameter files, and results.

## Materials

- Environments with metabolite concentrations: stored as `.csv` files in `media` folder, collected from AGORA v1.03
- Genome-wide metabolic profiles of bacteria in the gut: `.xml` files in SBML format, compressed in `Agora/dataset/AGORA-1.03.zip`

## Simulation folders

Description of each experiment can be found in `experiments.md` in repos "metabolic-micom_records" or "metabolic-micom_doc".

Each folder contains:

- Running script: `run.sh`. The scripts were not written to be run automatically and robustly in any environment. Reading the script and changing some of its elements before running are required. At the moment, its main purpose is recording all that have been done.
- Parameter files in `parameters` folder. Each file is in `.txt` format, the first line is the simulation ID, followed by paths to media files, and then paths to SBML files.
- Simulation results are stored in 2 folders. One has name in the format of `"$program-version"_growth_parameters`, the other is `"$program-version"_sim_"$simulation-ID"`. The former contains calibration parameters of each species, while the later contains culturing simulation results.

