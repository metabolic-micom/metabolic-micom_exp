# Herein, $COMMUNISM is an environment variable storing the full path of the program.
# To use this script in your machine, create one such variable or add a line in this script to create a local variable storing the program's location

# Activate environment with R=3.5.1
conda activate micom

#######################
# Checking if calibration parameter `maxOD` has any effect on culturing simulations
# Monoculture: Corynebacterium_ulcerans_809 in EU_avg
#######################

# Use parameter files and calibration results from "test_v2.10.0"
mkdir parameters
cp ../test_v2.10.0/parameters/param_test_plastic.txt ./parameters/
cp ../test_v2.10.0/parameters/param_test_sfm.txt ./parameters/

cp -r -u ../test_v2.10.0/v2.10.0_growth_parameters_EU_avg .

# View current calib parameter maxOD
awk '{IFS="\t"} {if ($1=="maxOD") print $2}' v2.10.0_growth_parameters_EU_avg/Corynebacterium_ulcerans_809/growth_curve_parameters.tsv
# 1.01657
# Change the value to 2.18199
cd v2.10.0_growth_parameters_EU_avg/Corynebacterium_ulcerans_809
awk '{IFS="\t"; OFS="\t"} {if ($1=="maxOD") 
                                print $1,"2.18199"; 
                            else print $1,$2;
                            }' growth_curve_parameters.tsv > growth_curve_parameters.new.tsv

cat growth_curve_parameters.new.tsv # checking

mv growth_curve_parameters.new.tsv growth_curve_parameters.tsv # replace

cd -

# Re-use the culturing simulation folders - to use same random seeds
cp -r ../test_v2.10.0/v2.10.0_sim_test_plastic.txt .
cp -r ../test_v2.10.0/v2.10.0_sim_test_sfm.txt .

#######################
# Use program version 2.10.0, on branch "v2.10.0_plastic" <=> Plasticity mode with 20 flux modes
program_location=${COMMUNISM%/*}
cd $program_location
git checkout v2.10.0_plastic
cd -

$COMMUNISM ./parameters/param_test_plastic.txt > ./test_plastic.$(date '+%d%m%Y_%H:%M:%S').log 2>&1
    
#######################
# Use program version 2.10.0, on branch "v2.10.0_sfm" <=> SFM mode with 1 flux mode
program_location=${COMMUNISM%/*}
cd $program_location
git checkout v2.10.0_sfm
cd -

$COMMUNISM ./parameters/param_test_sfm.txt > ./test_sfm.$(date '+%d%m%Y_%H:%M:%S').log 2>&1
