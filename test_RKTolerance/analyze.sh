#######################
#######################
# Apr 19, 2021
# Model adjustment testing: increasing `Environment::RKTolerance`
# Coculture simulations 
# batch & continuous
# community: 
# simulationTime = 12.0
# program version: v2.10.0_plastic
#######################
####################### 

community_arr=("I_a_I_b" "III_b_IV_a" "II_b_III_b")

####################### 
# Extract running status, including SLURM log information
echo -e communityID"\t"case"\t"culture_type"\t"program_log_doneSim"\t"program_log_doneR"\t"slurm_log"\t"run_time"\t"max_mem_used > run_status.tsv

log_dir="./log"
slurm_dir="./slurm_log"
job_file="./jobs.tsv"

for log_file in $log_dir"/"*".log"; do

    log_filename=${log_file##*/};
    # separate elements of `log_filename` into an array, elements separated by "."
    IFS="." read -r -a log_filename_arr <<< "$log_filename"
    
    simID=${log_filename_arr[0]};
    communityID=${simID%_*};
    
    case=${log_filename_arr[1]};
    culture_type=${log_filename_arr[2]};
    
    program_log_doneSim=$(grep -c "*** All simulations finished ***" $log_file)
    
    check=$(tail -n 4 $log_file | head -n 1);
    if [[ $check == "*** Finished calling R scripts ***" ]]; then
        program_log_doneR=1;
    else
        program_log_doneR=0;
    fi;
    
    slurm_id=$(grep "log/${log_filename}" $job_file | cut -f1);
    slurm_log_file=$slurm_dir"/"$slurm_id".log";
    slurm_log=$(grep "State" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
    
    run_time=$(grep "Used walltime" $slurm_log_file | cut -d ":" --output-delimiter ":" -f2,3,4 | tr -d " ");
    
    max_mem_used=$(grep "Max Mem used" $slurm_log_file | cut -d ":" -f2 | tr -d " ");
    
    echo -e $communityID"\t"$case"\t"$culture_type"\t"$program_log_doneSim"\t"$program_log_doneR"\t"$slurm_log"\t"$run_time"\t"$max_mem_used >> run_status.tsv;

done
