#######################
#######################
# Apr 16, 2021
# Model adjustment testing: increasing `Environment::RKTolerance` to 1.0e-5 and 1.0e-4 (originally, 1.0e-6)
# Coculture simulations 
# batch & continuous
# community: 
# simulationTime = 12.0
# program version: v2.10.0_plastic
#######################
#######################

community="I_a_I_b"
sp_names=("Helicobacter_pylori_26695" "Pseudomonas_nitroreducens_HBP1")
sp_IDs=("setA_15" "setA_21")

community="III_b_IV_a"
sp_names=("Escherichia_coli_str_K_12_substr_MG1655" "Odoribacter_laneus_YIT_12061")
sp_IDs=("setA_13" "setA_19")

community="II_b_III_b"
sp_names=("Ruminococcus_lactaris_ATCC_29176" "Escherichia_coli_str_K_12_substr_MG1655")
sp_IDs=("setA_24" "setA_13")

cases=("RKTo6" "RKTo5" "RKTo4") # respectively corresponding to values of `Environment::RKTolerance`: 1.0e-6; 1.0e-5; and 1.0e-4

new_case="RKTo6a" # continuous culture is not stopped

#######################
# Control: `Environment::RKTolerance` = 1.0e-6
# This is the "original" value
# Simulations with this control value were already run and stored in `setA_coculture_12h/rep1/` (on Peregrine)
parent=$MICOM_EXP"setA_coculture_simTime12h/rep1/v2.10.0_sim_"$community"_plastic/"

cp -r $parent .

simID=$community"_plastic.RKTo6" # assign new simulation ID
mv "v2.10.0_sim_"$community"_plastic/" "v2.10.0_sim_"$simID

#######################
# Prepare parameter files
mkdir parameters/

param_dir="./parameters"

parent=$MICOM_EXP"setA_coculture_simTime12h/parameters"
old_simID=$community"_plastic"

# For cases in the following order: RKTo6, RKTo5, and RKTo4
for c in ${cases[@]}; do
    
    new_simID=$old_simID"."$c;
    
    param_file=$param_dir"/param_"$new_simID".txt";
    
    echo $new_simID > $param_file;
    
    tail -n +2 $parent"/param_"$old_simID".txt" >> $param_file; 
done

# For new case: RKTo6a. Applied to community III_b_IV_a
community="III_b_IV_a"
new_simID=$old_simID"."$new_case
param_file=$param_dir"/param_"$new_simID".txt"

echo $new_simID > $param_file
grep 'EU_avg' $parent"/param_"$old_simID".txt" >> $param_file
grep 'mediterranean' $parent"/param_"$old_simID".txt" >> $param_file
tail -n 2 $parent"/param_"$old_simID".txt" >> $param_file

#######################
# Prepare calibration files
mkdir v2.10.0_growth_parameters_EU_avg

calib_dir="v2.10.0_growth_parameters_EU_avg"

parent=$MICOM_EXP"setA_coculture_simTime12h/rep1/"$calib_dir

for sp in ${sp_names[@]}; do
    cp -r $parent"/"$sp "./"$calib_dir;
done

#######################
# Running (on Peregrine)
mkdir log/
mkdir slurm_log/

log_dir="./log"
slurm_dir="./slurm_log"
param_dir="./parameters"

culture_type_arr=("batch" "continuous")

# For cases in the following order: RKTo6, RKTo5, and RKTo4
for i in ${!cases[@]}; do
    c=${cases[$i]};
    simID=$community"_plastic."$c;
    
    if [ $i -gt 0 ]; then
    
        program=$MICOM"v2.10.0_plastic."$c"/communism";
        param_file=$param_dir"/param_"$simID".txt";
        
        mkdir -p "v2.10.0_sim_"$simID"/experiments/";
        cp -r "v2.10.0_sim_"$controlID"/experiments/seeds/" "v2.10.0_sim_"$simID"/experiments/";
        
        tail -n +2 $param_file > "v2.10.0_sim_"$simID"/loaded_parameters.txt";
    
        for cult in ${culture_type_arr[@]}; do
            
            log_file=$log_dir"/"$simID"."$cult".log";
            coculture_type="coculture_"$cult;
            
            sbatch $COMMUNISM_JOB $program $param_file $log_file $coculture_type;
        done;
        
    else # for simulations that are already run
        param_file=$param_dir"/param_"$simID".txt";
        
        parent=$MICOM_EXP"setA_coculture_simTime12h/rep1";
        parent_logdir=$parent"/log";
        parent_jobs=$parent"/jobs.tsv";
        parent_slurmdir=$parent"/slurm_log";
        
        for cult in ${culture_type_arr[@]}; do
            
            # Copy log file
            log_file=$log_dir"/"$simID"."$cult".log";
            parent_logfile=${simID%.*}"."$cult".rep1.log";
        
            cp $parent_logdir"/"$parent_logfile $log_dir;
            mv $log_dir"/"$parent_logfile $log_file;

            # Copy SLURM log file
            slurm_id=$(grep "log/$parent_logfile" $parent_jobs | cut -f1);
            
            slurm_file=$slurm_id".log";
            cp $parent_slurmdir"/"$slurm_file $slurm_dir;
            
            # Write to jobs.tsv
            echo -e $slurm_id"\t"$param_file"\t"$log_file >> ./jobs.tsv;
            
        done;
        
        controlID=$simID; 
    fi;
done

# For new case: RKTo6a. Applied to community III_b_IV_a
# Only tested with continuous culture
community="III_b_IV_a"
simID=$community"_plastic."$new_case
controlID=$community"_plastic.RKTo6"

program=$MICOM"v2.10.0_plastic."$new_case"/communism"
param_file=$param_dir"/param_"$simID".txt"

mkdir -p "v2.10.0_sim_"$simID"/experiments/"
cp -r "v2.10.0_sim_"$controlID"/experiments/seeds/" "v2.10.0_sim_"$simID"/experiments/"

tail -n +2 $param_file > "v2.10.0_sim_"$simID"/loaded_parameters.txt"

log_file=$log_dir"/"$simID".continuous.log"
coculture_type="coculture_continuous"

sbatch $COMMUNISM_JOB $program $param_file $log_file $coculture_type

#######################
# Check hardcoded_parameters
echo -e communityID"\t"case"\t"flowRate"\t"NFluxModes"\t"fluxNGAM"\t"SwitchRate"\t"SwitchSensitivity"\t"numReps"\t"simTime"(h)\t""initDens(a.u.)" > hardcoded_parameters.tsv

log_dir="./log"
    
for log_file in $log_dir"/"*".log"; do
    log_filename=${log_file##*/};
    # separate elements of `log_filename` into an array, elements separated by "."
    IFS="." read -r -a log_filename_arr <<< "$log_filename"
    
    simID=${log_filename_arr[0]};
    communityID=${simID%_*};
    
    case=${log_filename_arr[1]};

    sim_dir="./"$rep_name"/v2.10.0_sim_"$simID"."$case;
    data=$(tail -n +2 $sim_dir"/hardcoded_parameters.tsv" | sed -e 's/\t/\\t/g');
    
    echo -e $communityID"\t"$case"\t"$data >> hardcoded_parameters.tsv;
done

#Check
cut -f4 hardcoded_parameters.tsv | sort | uniq
cut -f9 hardcoded_parameters.tsv | sort | uniq
