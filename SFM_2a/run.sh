# Herein, $COMMUNISM is an environment variable storing the full path of the program.
# To use this script in your machine, create one such variable or add a line in this script to create a local variable storing the program's location

# Activate environment with R=3.5.1
conda activate micom

#######################
# Use program version 2.09.1, on branch "main" => Plasticity is allowed
program_location=${COMMUNISM%/*}
cd $program_location
git checkout main
cd -

# 1plastic. Monoculture test: Bifidobacterium_longum_E18 + EU_avg
$COMMUNISM ./parameters/param_sfm2a_1plastic.txt > ./sfm2a_1plastic.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

#######################
# Use program version 2.09.1, on branch "SFM_2a"
program_location=${COMMUNISM%/*}
cd $program_location
git checkout SFM_2a
cd -
    
# Experiments
# 1. Monoculture test: Bifidobacterium_longum_E18 + EU_avg
$COMMUNISM ./parameters/param_sfm2a_1.txt > ./sfm2a_1.$(date '+%d%m%Y_%H:%M:%S').log 2>&1
