# Herein, $COMMUNISM is an environment variable storing the full path of the program.
# To use this script in your machine, create one such variable or add a line in this script to create a local variable storing the program's location

# Activate environment with R=3.5.1
conda activate micom

# Testing
$COMMUNISM ./parameters/param_set_1.txt > ./sim_set_1.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

$COMMUNISM ./parameters/param_set_1a.txt > ./sim_set_1a.$(date '+%d%m%Y_%H:%M:%S').log 2>&1

$COMMUNISM ./parameters/param_set_1b.txt > ./sim_set_1b.$(date '+%d%m%Y_%H:%M:%S').log 2>&1
